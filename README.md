Webificaton Example for Sterling
================================

This is a trivial example of one possible way to webify Sterling’s game code.

## Example "Game"

The example "game" is as follows:

* on the screen are drawn a circle, and two buttons

* the user pushes the buttons to make the circle bigger or smaller

* the "AI" picks the color, which will be changed on every round

## Structure of the Example

The example is built using [Flask](https://flask.palletsprojects.com/), a fairly lightweight, Python web server.
A separate thread is running corresponding to
Sterling’s exisiting game code. Since the goal is to reuse the existing game code’s graphics, it creates an image
(in this case in SVG) which it passes to the web server to use when updating the user’s browser. The web server
and the game code communitate using a pair of synchronized queues.

No AJAX or JavaScript is used in this example, which I hope will minimize the amount of learning required
to extend it. I hope this is sufficient for your needs.

This example is very much single user. Considerably more work will be required to make it suitable for production
use, but I hope it’s enough to give the idea.

## Running the Example

Some sort of virual environment is recommended, as is a recent version of Python 3 (it was written using Python 3.9).
First do, in the venv and cd'd to this directory, `pip install -r requirements.txt`. That should get Flask installed.

Then do `FLASK_APP=game.py FLASK_ENV=development python -m flask run` to start it. While I've been using
Linux, I think that will also work in macOS. I've no idea what the corresponding incantation would be in Windoze.
Hell, I have trouble just figuring out how to shut down Windoze machines.

Now point a web browser at `http://localhost:5000/`. You should see the circle and buttons. Push away.

## Guided Tour

The code proper is in `game.py`. The code mimicing the existing game code is that for the `Game` object.
It first grabs what ever button press, if any, is pending from the UI via the web server, updates a global state
with the result of that choice (or initializes it the first time, before any button has been pressed), then
calls the "AI" to update the color (which is really just a random choice of a color subject to the contraint that
it's different from the current one), then builds an SVG and sends it to the web server.

The web server stuff is just the (decorated) function `play_game()`. It reads the button press, if any,
sends it to the game thread, waits for an SVG back, and displays the HTML page with that SVG embedded into it.

The HTML page is built from a template in `templates/game.html`. There’s a near-vacuous file of CSS stuff
in `static/game.css`.
