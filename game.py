from flask import Flask, render_template, request
import queue
import random
import threading

app = Flask(__name__)

COLORS = ["red", "blue", "green", "yellow", "cyan", "magenta", "pink", "violet", "tan"]
MAX_SIZE = 90
MIN_SIZE = 10
INCREMENT = 10

pending_choice = None
choice_queue = queue.Queue()
svg_queue = queue.Queue()
game_thread = None


class Game(threading.Thread):

    def __init__(self):
        super().__init__(daemon=True)

    def run(self):
        state = initial_state()
        while True:
            choice = choice_queue.get()
            if not choice:
                state = initial_state()
            update_size(state, choice)
            update_color(state)
            svg_queue.put(make_svg(state["size"], state["color"]))


def initial_state():
    return {"color": COLORS[0], "size": (MAX_SIZE + MIN_SIZE) // 2}

def update_size(state, choice):
    if choice == "larger":
        state["size"] = min(state["size"] + INCREMENT, MAX_SIZE)
    elif choice == "smaller":
        state["size"] = max(state["size"] - INCREMENT, MIN_SIZE)

def update_color(state):
    unused_colors = list(COLORS)
    unused_colors.remove(state["color"])
    state["color"] = random.choice(unused_colors)

def make_svg(size, color):
   return f"<svg height=200 width=200 fill={color}><circle cx=100 cy=100 r={size} stroke={color}/></svg>"

@app.route("/")
def play_game():
    global game_thread
    if not game_thread:
        game_thread = Game()
        game_thread.start()
    choice_queue.put(request.args.get("choice", ""))
    return render_template("game.html", svg=svg_queue.get())
